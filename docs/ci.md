# Process
## Code Review
## Build
## Static Check
## Unit Test
## Integration Test
## CDC Test
## Publish

![testPyramid](img/testPyramid.png)
[Test Pyramid](https://martinfowler.com/articles/practical-test-pyramid.html)

# Tooling
* GitLab CI / TFS
* Gradle / Maven / make
* SpringBoot
* SonarQube

Example:
![JavaPluginTasks](img/javaPluginTasks.png)
[JavaPlugin](https://docs.gradle.org/current/userguide/java_plugin.html)