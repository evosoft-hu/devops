# Clean Code Syllabus

[DevOps Training Source](https://gitlab.com/evosoft-hu/devops) project generates [DevOps Training Pages](https://evosoft-hu.gitlab.io/devops)

The content serves as **training material**.

If you require something please leave an [Issue](https://gitlab.com/evosoft-hu/devops/issues)

# Technical Details

Static web content generator used: [mkdocs](http://mkdocs.org).
Publisher and Content Provider: GitLab [Pages](https://docs.gitlab.com/ce/user/project/pages).